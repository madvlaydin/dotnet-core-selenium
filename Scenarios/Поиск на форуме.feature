﻿Функционал: Поиск на форуме

Сценарий: Поиск на форуме и генерация locators.json
	Допустим я перешел на 'https://automated-testing.info/'
	Когда я ввёл в поиск 'xpath @madvlaydin'
	И выбрал первый результат
	Тогда в этом посте
	| key              | value             |
	| first username   | madvlaydin        |
	| second full-name | Vladislav Abramov |
	