$s = Get-WmiObject Win32_Process | Where-Object { $_.commandline -match '--enable-logging' } | Select-Object processid
Stop-Process -Id $s.processid

$s = Get-WmiObject Win32_Process | Where-Object { $_.commandline -match 'IEDriverServer' } | Select-Object processid
Stop-Process -Id $s.processid

$s = Get-WmiObject Win32_Process | Where-Object { $_.commandline -match 'IEXPLORE.EXE' } | Select-Object processid
Stop-Process -Id $s.processid

Stop-Process -name chromedriver

Remove-Item -Path .\allure-results -Recurse
Remove-Item -Path .\allure-report -Recurse
Remove-Item -Path .\bin -Recurse
Remove-Item -Path .\obj -Recurse
Remove-Item -Path .\TestResult.xml

dotnet build
