﻿using AutomatedTestingInfo.Helpers;
using AutomatedTestingInfo.Pages;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Steps
{
    [Binding]
    public class ПоискНаФорумеSteps
    {
        private readonly ScenarioContext context;
        private readonly Forum forum;

        public ПоискНаФорумеSteps(ScenarioContext scenarioContext)
        {
            context = scenarioContext;

            forum = new Forum(context);
        }

        [Given(@"я перешел на '(.*)'")]
        public void ДопустимЯПерешелНа(string url)
        {
            context.Driver().Navigate().GoToUrl(url);
        }

        [When(@"я ввёл в поиск '(.*)'")]
        public void ЕслиЯВвёлВПоиск(string text)
        {
            forum.Search(text, context);
        }

        [When(@"выбрал первый результат")]
        public void ЕслиВыбралПервыйРезультат()
        {
            forum.SelectFirstSearchItem(context);
        }

        [Then(@"в этом посте")]
        public void ТоВЭтомПосте(Table table)
        {
            context.AddTable(table);

            forum.CheckTopic(context);
        }
    }
}
