﻿using AutomatedTestingInfo.Helpers;
using FluentAssertions;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Pages
{
    public class Forum
    {
        public Forum(ScenarioContext context)
        {
            PageFactory.InitElements(context.Driver(), this);
        }

        [FindsBy(How = How.XPath, Using = "//div[@class = 'title']")]
        private IWebElement SiteLogo { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id = 'toggle-hamburger-menu']")]
        private IWebElement Hamburger { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href = '/latest']")]
        private IWebElement Latest { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href = 'https://testomat.io']")]
        private IWebElement Testomat { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id = 'search-button']")]
        private IWebElement SearchButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id = 'search-term']")]
        private IWebElement SearchTerm { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class = 'search-result-topic'] //li[@class = 'item']")]
        private IList<IWebElement> SearchItems { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class = 'topic-meta-data'] // span[@class = 'first username']")]
        private IWebElement TopicAuthorUsername { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class = 'topic-meta-data'] // span[@class = 'second full-name']")]
        private IWebElement TopicAuthorFullname { get; set; }

        public void Search(string text, ScenarioContext context)
        {
            SiteLogo.Log(context);
            Hamburger.Log(context);
            Latest.Log(context);
            Testomat.Log(context);

            SearchButton.Log(context).Click();

            SearchTerm.Log(context).SendKeys(text);
        }

        public void SelectFirstSearchItem(ScenarioContext context)
        {
            SearchItems.ToList().First().Log(context).Click();
        }

        public void CheckTopic(ScenarioContext context)
        {
            TopicAuthorUsername.Log(context).Text.Should().Be(context.Value("first username"));

            TopicAuthorFullname.Log(context).Text.Should().Be(context.Value("second full-name"));
        }
    }
}
