using Allure.Commons;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using Newtonsoft.Json;
using AutomatedTestingInfo.Helpers;

namespace AutomatedTestingInfo.Helpers
{
    public class Allure
    {
        /// <summary>
        /// Добавление скрина в шаг с ошибкой, если тест упал
        /// </summary>
        public static void MakeScreenshot(ScenarioContext context)
        {
            Screenshot ss = ((ITakesScreenshot)context["driver"]).GetScreenshot();

            string testName = TestContext.CurrentContext.Test.Name;

            string screenshotName = DateTime.Now.TimeOfDay.ToString().Replace(":", "-").Substring(0, 8) + " " + testName + ".png";

            ss.SaveAsFile($"{AllureLifecycle.Instance.ResultsDirectory}\\" + screenshotName, ScreenshotImageFormat.Png);

            AllureLifecycle.Instance.UpdateTestCase((string)context["AllureUUID"], tc =>
            {
                int i = tc.steps.FindIndex(step => step.status != Status.passed && step.status != Status.skipped);
                // Ищем шаг, статус которого не равен успешному и пропущенному. Такой будет 1 и может быть красным либо фиолетовым

                tc.steps[i].attachments.Add(new Attachment
                {
                    name = "Скриншот ошибки",
                    source = screenshotName
                });
            });
        }

        /// <summary>
        /// Просто сделать скрин и добавить его в текущий шаг
        /// </summary>
        public static void MakeScreenshot(ScenarioContext context, string screenshotName)
        {
            Screenshot ss = ((ITakesScreenshot)context["driver"]).GetScreenshot();

            string testName = TestContext.CurrentContext.Test.Name;

            string fileName = DateTime.Now.TimeOfDay.ToString().Replace(":", "-").Substring(0, 8) + " " + testName + ".png";

            ss.SaveAsFile($"{AllureLifecycle.Instance.ResultsDirectory}\\" + fileName, ScreenshotImageFormat.Png);

            AllureLifecycle.Instance.UpdateStep(step =>
            { 
                step.attachments.Add(new Attachment
                {
                    name = screenshotName,
                    source = fileName
                });
            });
        }

        public static void LocatorsToJSON(List<Locator> locators, ScenarioContext context)
        {
            var json = JsonConvert.SerializeObject(locators);

            string jsonName = DateTime.Now.TimeOfDay.ToString().Replace(":", "-").Substring(0, 8) + " " + TestContext.CurrentContext.Test.Name + ".json";

            System.IO.File.WriteAllText($"{AllureLifecycle.Instance.ResultsDirectory}\\" + jsonName, json);

            AllureLifecycle.Instance.UpdateTestCase((string)context["AllureUUID"], tc =>
            {
                tc.attachments.Add(new Attachment
                {
                    name = "locators2",
                    source = jsonName
                });
            });
        }
    }
}

