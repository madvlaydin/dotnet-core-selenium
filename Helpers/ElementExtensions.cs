using AutomatedTestingInfo.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Helpers
{
    public static class ElementExtensions
    {
        /// <summary>
        /// Навести курсор на элемент и кликнуть
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="context"></param>
        public static void MouseOverAndClick(this IWebElement elem, ScenarioContext context)
        {
            Actions act = new Actions(context.Driver());

            act.MoveToElement(elem).Click().Build().Perform();
        }

        /// <summary>
        /// Ожидание кликабельности элемента
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IWebElement WaitClickable(this IWebElement elem, ScenarioContext context)
        {
            var wait = new WebDriverWait(context.Driver(), new TimeSpan(0, 0, 20));
            var clickableElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(elem));
            return clickableElement;
        }

        /// <summary>
        /// Нажать Home в текстовом поле
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        public static IWebElement PressHome(this IWebElement elem)
        {
            elem.Click();

            elem.SendKeys(Keys.Home);

            return elem;
        }

        /// <summary>
        /// Нажать End в текстовом поле
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        public static IWebElement PressEnd(this IWebElement elem)
        {
            elem.Click();

            elem.SendKeys(Keys.End);

            return elem;
        }

        /// <summary>
        /// Очистить поле (ctrl + a → backspace)
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        public static IWebElement ClearField(this IWebElement elem)
        {
            elem.SendKeys(Keys.Control + "a");

            elem.SendKeys(Keys.Backspace);

            return elem;
        }

        /// <summary>
        /// Заполнить текстовое поле как надо:
        /// Если пришла пустая строка → просто очистить поле
        /// Если не пустая → ввести текст после очищения
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static IWebElement SetValue(this IWebElement elem, string text)
        {
            elem.ClearField();

            if (!String.IsNullOrEmpty(text)) elem.SendKeys(text);

            return elem;
        }

        /// <summary>
        /// Упрощенная проверка string на пустоту
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool Empty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        /// <summary>
        /// Клик в элемент с возвратом элемента, чтобы с ним можно было дальше работать
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        public static IWebElement Click(this IWebElement elem)
        {
            elem.Click();

            return elem;
        }

        /// <summary>
        /// Очистить поле и ввести текст
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static IWebElement ClearType(this IWebElement elem, string text)
        {
            elem.Clear();
            elem.SendKeys(text);

            return elem;
        }

        /// <summary>
        /// Проверить существование элемента
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool Exist(this IWebElement elem, ScenarioContext context)
        {
            bool exist;
            context.Driver().Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);

            try
            {
                exist = elem.Displayed;
            }
            catch (Exception)
            {
                exist = false;
            }

            context.Driver().Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(4);

            return exist;
        }

        /// <summary>
        /// Содержит ли элемент текст (ToLower к обоим текстам)
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool ContainsText(this IWebElement elem, string text)
        {
            return elem.Text.ToLower().Contains(text.ToLower());
        }

        /// <summary>
        /// Ожидание появления текста в элементе, дефолт 5 секунд
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="text"></param>
        /// <param name="waitTime"></param>
        /// <returns></returns>
        public static IWebElement WaitContainsText(this IWebElement elem, string text, int waitTime = 5)
        {
            var start = DateTime.Now;

            while (!elem.ContainsText(text))
            {
                Thread.Sleep(400);
                
                if ((DateTime.Now - start).TotalSeconds > waitTime)
                    throw new Exception($"Ожидаемый в элементе текст \"{text}\" не появился за {waitTime} секунд.");
            }

            return elem;
        }

        /// <summary>
        /// Получение абсолютного xpath элемента
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetAbsoluteXPath(this IWebElement elem, ScenarioContext context)
        {
            var driver = context.Driver();

            var a =  (string)((IJavaScriptExecutor)driver).ExecuteScript(
                 "function absoluteXPath(element) {" +
                   "var comp, comps = [];" +
                     "var parent = null;" +
                     "var xpath = '';" +
                     "var getPos = function(element) {" +
                     "var position = 1, curNode;" +
                     "if (element.nodeType == Node.ATTRIBUTE_NODE) {" +
                     "return null;" +
                     "}" +
                     "for (curNode = element.previousSibling; curNode; curNode = curNode.previousSibling) {" +
                         "if (curNode.nodeName == element.nodeName) {" +
                             "++position;" +
                         "}" +
                     "}" +
                     "return position;" +
                     "};" +

                     "if (element instanceof Document) {" +
                         "return '/';" +
                     "}" +

                     "for (; element && !(element instanceof Document); element = element.nodeType == Node.ATTRIBUTE_NODE ? element.ownerElement : element.parentNode) {" +
                     "comp = comps[comps.length] = {};" +
                     "switch (element.nodeType) {" +
                     "case Node.TEXT_NODE:" +
                         "comp.name = 'text()';" +
                         "break;" +
                     "case Node.ATTRIBUTE_NODE:" +
                         "comp.name = '@' + element.nodeName;" +
                         "break;" +
                     "case Node.PROCESSING_INSTRUCTION_NODE:" +
                         "comp.name = 'processing-instruction()';" +
                         "break;" +
                     "case Node.COMMENT_NODE:" +
                         "comp.name = 'comment()';" +
                         "break;" +
                     "case Node.ELEMENT_NODE:" +
                         "comp.name = element.nodeName;" +
                         "break;" +
                     "}" +
                     "comp.position = getPos(element);" +
                     "}" +

                     "for (var i = comps.length - 1; i >= 0; i--) {" +
                         "comp = comps[i];" +
                         "xpath += '/' + comp.name.toLowerCase();" +
                         "if (comp.position !== null) {" +
                         "xpath += '[' + comp.position + ']';" +
                         "}" +
                     "}" +
                     "return xpath;" +

                 "} return absoluteXPath(arguments[0]);", elem);

            return a.Replace("/html[1]/body[1]/", "//");
        }

        /// <summary>
        /// Выдернуть xpath из декларации элемента в *Page.cs
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        public static string GetBysFromPageFactoryElements(this IWebElement elem)
        {
            var path = (IList<By>)elem.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic).ToList().Single(prop => prop.Name == "Bys").GetValue(elem);

            string By = path[0].ToString().Replace("By.XPath: ", "");
            
            return By;
        }

        /// <summary>
        /// Залогировать элемент и путь до него, чтоб потом с помощью экстеншна в браузере увидеть, куда кликал тест
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IWebElement Log(this IWebElement elem, ScenarioContext context)
        {
            new Locator(elem, context);

            return elem;
        }

        /// <summary>
        /// Получение потомка элемента
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="context"></param>
        /// <param name="locator"></param>
        /// <returns></returns>
        public static IWebElement Descendant(this IWebElement elem, ScenarioContext context, string locator)
        {
            var path = elem.GetAbsoluteXPath(context) + $"/descendant::{locator}";

            return context.Driver().FindElement(By.XPath(path));
        }

        /// <summary>
        /// Если текст не пустой, то выбрать его из выпадающего списка
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="text"></param>
        /// <param name="context"></param>
        public static void SelectOption(this IWebElement elem, string text, ScenarioContext context)
        {
            if (!text.Empty())
            { 
                elem.Click();

                Thread.Sleep(500);

                context.Driver().FindElements(By.XPath("//li[@role = 'option']")).ToList().First(elem => elem.Text.Contains(text)).Click();
            }
        }
    }
}

