using Allure.Commons;
using BoDi;
using AutomatedTestingInfo.Helpers;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Helpers
{
    [Binding]
    public class Hooks
    {
        private Browser browser;
        private readonly IObjectContainer container;
        private readonly ScenarioContext context;
        [ThreadStatic] public static List<Locator> locators;

        public Hooks(IObjectContainer objectContainer, ScenarioContext scenarioContext)
        {
            container = objectContainer;
            context = scenarioContext;
        }

        [BeforeScenario]
        public void Setup()
        {
            browser = new Browser(context);
            container.RegisterInstanceAs(browser);
            locators = new List<Locator>();
        }

        [BeforeStep]
        public void BeforeStep()
        {
            if (context.Value("AllureUUID").Empty()) AllureLifecycle.Instance.UpdateTestCase(tc => context["AllureUUID"] = tc.uuid);
        }
        
        [AfterScenario]
        public void TearDown()
        {
            if (context.TestError != null) Allure.MakeScreenshot(context);

            Allure.LocatorsToJSON(locators, context);

            browser.Termination();
        }
    }
}
