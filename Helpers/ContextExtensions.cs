using OpenQA.Selenium;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Helpers
{
    public static class ContextExtensions
    {
        /// <summary>
        /// Берём текущий браузер из контекста теста
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IWebDriver Driver(this ScenarioContext context)
        {
            return (IWebDriver)context["driver"];
        }

        /// <summary>
        /// Вернуть значение из контекста по ключу, либо null в случае ошибки
        /// </summary>
        /// <param name="context"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Value(this ScenarioContext context, string key)
        {
            try
            {
                return (string)context[key];
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Кладём таблицу из шага Specflow в контекст, чтобы иметь возможность по ключу (и его наличию) получать и работать со значениями
        /// </summary>
        /// <param name="context"></param>
        /// <param name="table"></param>
        public static void AddTable(this ScenarioContext context, Table table)
        {
            table.Rows.ToList().ForEach(row => context[row["key"]] = row["value"]);
        }
    }
}

