using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Helpers
{
    public class Browser
    {
        public IWebDriver driver;

        private enum BrowserName { Chrome, IE, Edge}

        public Browser(ScenarioContext context)
        {
            Start();

            context["driver"] = driver;
        }

        private void Start()
        {
            var browser = BrowserName.Chrome;

            switch (browser)
            {
                case BrowserName.Chrome:

                    driver = GetChromeDriver();

                    break;

                case BrowserName.IE:

                    driver = GetIEDriver();

                    break;

                case BrowserName.Edge:
                    break;

                default:
                    driver = GetChromeDriver();
                    break;
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }

        public void Termination()
        {
            driver.Quit();
        }

        private IWebDriver GetChromeDriver()
        {
            var opt = new ChromeOptions();

            opt.AddArgument("window-size=1920,1080");
            opt.AddArgument("enable-automation");
            opt.AddArgument("no-sandbox");
            opt.AddArgument("disable-notifications");
            opt.AddArgument("ignore-certificate-errors");

            opt.AddUserProfilePreference("credentials_enable_service", false);
            opt.AddUserProfilePreference("profile.password_manager_enabled", false);

            return new ChromeDriver(opt);
        }

        private IWebDriver GetIEDriver()
        {
            var ieOptions = new InternetExplorerOptions
            {
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                RequireWindowFocus = false,
                IgnoreZoomLevel = true,
                UsePerProcessProxy = true,
                EnsureCleanSession = true,
                BrowserCommandLineArguments = "-private",
            };

            return new InternetExplorerDriver(ieOptions);
        }
    }
}

