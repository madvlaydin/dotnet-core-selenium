using OpenQA.Selenium;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace AutomatedTestingInfo.Helpers
{
    public class Locator
    {
        public List<string>                 urls        { get; set; }
        public string                       fullPath    { get; set; }
        public Dictionary<string, string>   tests       { get; set; }

        public Locator(IWebElement e, ScenarioContext context)
        {
            var driver = (IWebDriver)context["driver"];

            urls = new List<string>
            {
                driver.Url
            };

            try
            {
                fullPath = e.GetBysFromPageFactoryElements();
            }
            catch
            {
                fullPath = e.GetAbsoluteXPath(context);
            }

            tests = new Dictionary<string, string>
            {
                { context.ScenarioInfo.Title, (string)context["AllureUUID"] }
            };

            Hooks.locators.Add(this);
        }
    }
}

